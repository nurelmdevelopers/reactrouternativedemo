const styles = {
  btnStyle: {
    width: 200,
    backgroundColor: "#E94949",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    marginTop: 10
  }
};

export default styles;
