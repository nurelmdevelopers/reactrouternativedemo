import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
  return (
    <View>
      <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Header</Text>
    </View>
  );
};

export default Header;
