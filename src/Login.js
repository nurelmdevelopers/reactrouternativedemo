import React, { useState } from "react";
import { Text, View, TouchableHighlight } from "react-native";
import { Redirect } from "react-router-native";
import myDummyStore from "./myDummyStore";

const Login = () => {
  const [redirect, setRedirect] = useState(false)

  if (redirect || myDummyStore.isAuthenticated) return <Redirect to="/dashboard" />;

  return (
    <View>
      <Text>You must log in to view</Text>

      <TouchableHighlight
        style={{
          width: 200,
          backgroundColor: "#E94949",
          justifyContent: "center",
          alignItems: "center",
          padding: 10,
          marginTop: 10
        }}
        underlayColor="#f0f4f7"
        onPress={() => myDummyStore.authenticate(() => setRedirect(true))}
      >
        <Text>Log in</Text>
      </TouchableHighlight>
    </View>
  );
}

export default Login;
