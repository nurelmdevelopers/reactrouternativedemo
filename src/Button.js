import React from 'react';
import { Text, TouchableHighlight } from 'react-native';
import styles from './styles';

const Button = ({ onPress, text }) => {
  return (
    <TouchableHighlight
      style={styles.btnStyle}
      underlayColor="#f0f4f7"
      onPress={onPress}
    >
      <Text>{text}</Text>
    </TouchableHighlight>
  );
};

export default Button;
