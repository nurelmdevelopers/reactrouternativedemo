import React from "react";
import { Text, View, TouchableHighlight } from "react-native";
import { withRouter } from "react-router-native";
import myDummyStore from "./myDummyStore";

const AuthButton = withRouter(({ history }) =>
  myDummyStore.isAuthenticated ? (
    <View>
      <Text>Welcome!</Text>
      <TouchableHighlight
        style={{
          width: 200,
          backgroundColor: "#E94949",
          justifyContent: "center",
          alignItems: "center",
          padding: 10,
          marginTop: 10
        }}
        underlayColor="#f0f4f7"
        onPress={() => {
          myDummyStore.signout(() => history.push("/"));
        }}
      >
        <Text>Sign out</Text>
      </TouchableHighlight>
    </View>
  ) : (
    <Text>You are not logged in.</Text>
  )
);

export default AuthButton;
