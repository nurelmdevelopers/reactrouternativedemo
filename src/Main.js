import React from 'react';
import { Route, withRouter, Redirect, Link } from 'react-router-native';
import { View, Text, TouchableHighlight } from 'react-native';
import Login from './Login';
import Button from './Button';
import styles from './styles';

const myDummyStore = {
  isAuthenticated: false,
  authenticate(cb) {
    this.isAuthenticated = true;
    setTimeout(cb, 100); // fake async
  },
  signout(cb) {
    this.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};

const MyLinks = () => (
  <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
    <Link to="/public" style={{ flex: 1, alignItems: "center", padding: 10 }} underlayColor="#f0f4f7">
      <Text>Public Page</Text>
    </Link>
    <Link to="/protected" style={{ flex: 1, alignItems: "center", padding: 10 }} underlayColor="#f0f4f7">
      <Text>Protected Page</Text>
    </Link>
  </View>
)

const Public = () => <Text style={{ fontSize: 20 }}>Public</Text>;

function Protected() {
  return <Text style={{ fontSize: 20 }}>Protected</Text>;
}

const AuthButton = withRouter(({ history }) => {
  if (myDummyStore.isAuthenticated) {
    return (
      <Button
        onPress={() => myDummyStore.signout(() => history.push("/"))}
        text="Sign Out"
      />
    );
  }
  return (
    <Button
      onPress={() => myDummyStore.authenticate(() => history.push("/"))}
      text="Sign In"
    />
  );
});

const ProtectedRoute = ({
  component: OriginalComponent, ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!myDummyStore.isAuthenticated) return <Redirect to="/" />;

        return <OriginalComponent {...props} />;
      }}
    />
  );
};

const Main = () => {
  return (
    <View style={{ marginTop: 25, padding: 10 }}>
      <Route path="/" component={AuthButton} />
      <Route path="/" component={MyLinks} />
      <Route exact path='/' component={Login} />
      <Route path="/public" component={Public} />
      <ProtectedRoute path="/protected" component={Protected} />
    </View>
  );
};

export default Main;
